package hk.com.novare.springjooq.todo.service;

import java.util.List;
import java.util.Map;

public interface TodoService {
    List<Map<String, Object>> all();

    Map<String, Object> byId(Long id);

    void add(Map<String, ?> map);

    void add(List<Map<String, ?>> map);

    List<Map<String, ?>> findBy(Long authorId);
}
