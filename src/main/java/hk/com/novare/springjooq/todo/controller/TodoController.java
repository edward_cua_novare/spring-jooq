package hk.com.novare.springjooq.todo.controller;

import hk.com.novare.springjooq.todo.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/todo")
public class TodoController {
    private final TodoService todoService;

    @Autowired
    public TodoController(final TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping("/all")
    public final List<Map<String, Object>> getAllTodos() {
        return this.todoService.all();
    }

    @GetMapping("/{id}")
    public final Map<String, Object> getById(@PathVariable final Long id) {
        return this.todoService.byId(id);
    }

    @GetMapping("/")
    public final List<Map<String, ?>> getByAuthor(final @RequestParam Long authorId) {
        return this.todoService.findBy(authorId);
    }

    @PostMapping("/")
    public final String add(final @RequestBody Map<String, ?> input) {
        this.todoService.add(input);
        return "Successfully added TODO";
    }

    @PostMapping("/batch")
    public final String add(final @RequestBody List<Map<String, ?>> batch) {
        this.todoService.add(batch);
        return "Successfully added TODOs";
    }
}
