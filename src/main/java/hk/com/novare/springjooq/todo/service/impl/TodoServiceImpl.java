package hk.com.novare.springjooq.todo.service.impl;

import hk.com.novare.springjooq.todo.repository.TodoRepository;
import hk.com.novare.springjooq.todo.service.TodoService;
import jooq.tables.records.TodoRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class TodoServiceImpl implements TodoService {
    private final TodoRepository todoRepository;

    @Autowired
    private TodoServiceImpl(final TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    @Override
    public final List<Map<String, Object>> all() {
        return this.todoRepository.all();
    }

    @Override
    public final Map<String, Object> byId(final Long id) {
        return this.todoRepository
            .byId(id)
            .intoMap();
    }

    @Override
    public final void add(final Map<String, ?> map) {
        final TodoRecord record = new TodoRecord();
        record.fromMap(map);
        this.todoRepository.add(record);
    }

    @Override
    public final void add(final List<Map<String, ?>> map) {
        this.todoRepository.add(
            map.stream()
                .map(m -> {
                    final TodoRecord record = new TodoRecord();
                    record.from(m);
                    return record;
                })
                .collect(Collectors.toList())
        );
    }

    @Override
    public final List<Map<String, ?>> findBy(final Long authorId) {
        return this.todoRepository
            .byAuthorId(authorId)
            .stream()
            .map(rec -> rec.intoMap())
            .collect(Collectors.toList());
    }
}
