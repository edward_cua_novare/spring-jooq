package hk.com.novare.springjooq.todo.repository.impl;

import hk.com.novare.springjooq.todo.repository.TodoRepository;
import jooq.tables.records.TodoRecord;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

import static jooq.tables.Todo.TODO;

@Component
public class TodoRepositoryImpl implements TodoRepository {
    private final DSLContext dsl;

    @Autowired
    public TodoRepositoryImpl(final DSLContext dsl) {
        this.dsl = dsl;
    }

    @Override
    public final List<Map<String, Object>> all() {
        return this.dsl
            .select(
                TODO.DETAILS,
                TODO.ID,
                this.dsl
                    .select(TODO.ID.max())
                    .from(TODO)
                    .asField("idMax")
            )
            .from(TODO)
            .fetchMaps();
    }

    @Override
    public final TodoRecord byId(final Long id) {
        return this.dsl
            .select(TODO.fields())
            .from(TODO)
            .where(TODO.ID.eq(id))
            .fetchOneInto(TodoRecord.class);
    }

    @Override
    public final void add(final TodoRecord record) {
        this.dsl.executeInsert(record);
    }

    @Override
    public final void add(final List<TodoRecord> records) {
        this.dsl.batchInsert(records).execute();
    }

    @Override
    public final List<TodoRecord> byAuthorId(final Long authorId) {
        return this.dsl
            .select(TODO.fields())
            .from(TODO)
            .where(TODO.AUTHOR_ID.eq(authorId))
            .fetchInto(TodoRecord.class);
    }
}
