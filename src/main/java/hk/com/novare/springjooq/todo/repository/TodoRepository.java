package hk.com.novare.springjooq.todo.repository;

import jooq.tables.records.TodoRecord;

import java.util.List;
import java.util.Map;

public interface TodoRepository {
    List<Map<String, Object>> all();

    TodoRecord byId(Long id);

    void add(TodoRecord record);

    void add(List<TodoRecord> records);

    List<TodoRecord> byAuthorId(Long authorId);
}
