package hk.com.novare.springjooq.config;

import org.jooq.SQLDialect;
import org.jooq.impl.DataSourceConnectionProvider;
import org.jooq.impl.DefaultConfiguration;
import org.jooq.impl.DefaultDSLContext;
import org.jooq.impl.DefaultExecuteListenerProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;

import javax.sql.DataSource;

@Configuration
public class JooqConfiguration {
    private final DataSource dataSource;
    private final String sqlDialect;

    @Autowired
    public JooqConfiguration(
        final DataSource ds,
        @Value("${spring.jooq.sql-dialect}") final String sqlDialect
    ) {
        this.dataSource = ds;
        this.sqlDialect = sqlDialect;
    }

    @Bean
    public DataSourceConnectionProvider connectionProvider() {
        return new DataSourceConnectionProvider(
            new TransactionAwareDataSourceProxy(dataSource)
        );
    }

    @Bean
    public DefaultDSLContext dsl() {
        final DefaultConfiguration jooqConfiguration = new DefaultConfiguration();
        jooqConfiguration.setSQLDialect(SQLDialect.valueOf(this.sqlDialect));
        jooqConfiguration.set(connectionProvider());
        jooqConfiguration.set(
            new DefaultExecuteListenerProvider(
                new ExceptionTranslator()
            )
        );
        return new DefaultDSLContext(jooqConfiguration);
    }
}
