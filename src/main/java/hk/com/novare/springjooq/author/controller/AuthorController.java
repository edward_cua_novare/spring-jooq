package hk.com.novare.springjooq.author.controller;

import hk.com.novare.springjooq.author.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/author")
public class AuthorController {
    private final AuthorService authorService;

    @Autowired
    public AuthorController(final AuthorService authorService) {
        this.authorService = authorService;
    }

    @PostMapping("/")
    public final String addAuthor(@RequestBody final Map<String, ?> input) {
        this.authorService.add(input);
        return "Successfully added ";
    }

    @GetMapping("/")
    public final List<Map<String, ?>> search(@RequestParam final String keyword) {
        return this.authorService.search(keyword);
    }
}
