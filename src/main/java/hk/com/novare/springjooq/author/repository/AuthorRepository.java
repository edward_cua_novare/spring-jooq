package hk.com.novare.springjooq.author.repository;

import jooq.tables.records.AuthorRecord;

import java.util.List;

public interface AuthorRepository {
    void save(AuthorRecord record);

    List<AuthorRecord> search(String keyword);
}
