package hk.com.novare.springjooq.author.service.impl;

import hk.com.novare.springjooq.author.repository.AuthorRepository;
import hk.com.novare.springjooq.author.service.AuthorService;
import jooq.tables.records.AuthorRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class AuthorServiceImpl implements AuthorService {
    private final AuthorRepository authorRepository;

    @Autowired
    public AuthorServiceImpl(final AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    public final void add(final Map<String, ?> map) {
        final AuthorRecord record = new AuthorRecord();
        record.from(map);
        this.authorRepository.save(record);
    }

    @Override
    public final List<Map<String, ?>> search(final String keyword) {
        return this.authorRepository.search(keyword)
            .stream()
            .map(rec -> rec.intoMap())
            .collect(Collectors.toList());
    }
}
