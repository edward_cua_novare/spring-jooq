package hk.com.novare.springjooq.author.repository.impl;

import hk.com.novare.springjooq.author.repository.AuthorRepository;
import jooq.tables.records.AuthorRecord;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

import static jooq.Tables.AUTHOR;

@Component
public class AuthorRepositoryImpl implements AuthorRepository {
    private final DSLContext dsl;

    @Autowired
    public AuthorRepositoryImpl(final DSLContext dsl) {
        this.dsl = dsl;
    }

    @Override
    public final void save(final AuthorRecord record) {
        this.dsl.executeInsert(record);
    }

    @Override
    public final List<AuthorRecord> search(final String keyword) {
        return this.dsl
            .select(AUTHOR.fields())
            .from(AUTHOR)
            .where(
                AUTHOR.AGE.likeIgnoreCase("%" + keyword + "%")
                .or(AUTHOR.NAME.likeIgnoreCase("%" + keyword + "%"))
            )
            .fetchInto(AuthorRecord.class);
    }
}
