package hk.com.novare.springjooq.author.service;

import java.util.List;
import java.util.Map;

public interface AuthorService {
    void add(Map<String, ?> map);

    List<Map<String, ?>> search(String keyword);
}
